const { 
    writeToLowerCaseAppendName,
    writeToUpperCaseAppendName,
    readFileSortWriteAppendName,
    deleteFileInFileContent } = require('../problem2.js');

let uppercaseFile = './uppercase.txt';
let fileNames = './filenames.txt';
let lowerCaseFile = './lowercase.txt';
let sortedFile = './sorted.txt';

writeToUpperCaseAppendName('../lipsum.txt', uppercaseFile, fileNames, () => {
    writeToLowerCaseAppendName(uppercaseFile, lowerCaseFile, fileNames, () => {
        readFileSortWriteAppendName(lowerCaseFile, sortedFile, fileNames, () => {
            deleteFileInFileContent(fileNames);
        })
    });
});
const fs = require('fs');

function writeToUpperCaseAppendName(readFrom, writeTo, appendOn, callback = () => { }) {
    fs.readFile(readFrom, 'utf-8', function (err, fileContent) {
        if (err) {
            console.log(err);
        } else {
            fs.writeFile(writeTo, fileContent.toUpperCase(), 'utf-8', function (err) {
                if (err) {
                    console.log(err);
                } else {
                    fs.appendFile(appendOn, writeTo + '\n', 'utf-8', function (err) {
                        if (err) {
                            console.log(err);
                        } else {
                            callback();
                        }
                    })
                }
            })

        }
    })
}


function writeToLowerCaseAppendName(readFrom, writeTo, appendOn, callBack = () => { }) {
    fs.readFile(readFrom, 'utf-8', function (err, fileContent) {
        if (err) {
            console.log(err);
        } else {
            fs.writeFile(writeTo, fileContent.toLowerCase().split('.').join(''), 'utf-8', function (err) {
                if (err) {
                    console.log(err);
                } else {
                    fs.appendFile(appendOn, writeTo + '\n', 'utf-8', function (err) {
                        if (err) {
                            console.log(err);
                        } else {
                            callBack();
                        }
                    })
                }
                
            })

        }
    })
}


function readFileSortWriteAppendName(readFrom, writeTo, appendOn, callBack = () => { }) {
    fs.readFile(readFrom, 'utf-8', function (err, fileContent) {
        if (err) {
            console.log(err);
        } else {
            let sortedContent = fileContent.split(' ').sort().join(' ');
            //console.log(sortedContent);
            fs.writeFile(writeTo, sortedContent, 'utf-8', function (err) {
                if (err) {
                    console.log(err);
                } else {
                    fs.appendFile(appendOn, writeTo + '\n', 'utf-8', function (err) {
                        if (err) {
                            console.log(err);
                        } else {
                            callBack();
                        }
                    })
                }


            })

        }
    })
}

function deleteFileInFileContent(readFrom) {
    fs.readFile(readFrom, 'utf-8', function (err, fileNames) {
        if (err) {
            console.log(err);
        } else {
            console.log();
            fileNames.trim().split('\n').forEach((fileName) => {
                let path = fileName;
                fs.unlink(path, function (err) {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log('removed ' + path);
                    }
                })
            });
        }
    })
}

module.exports = {
    writeToLowerCaseAppendName,
    writeToUpperCaseAppendName,
    readFileSortWriteAppendName,
    deleteFileInFileContent
}
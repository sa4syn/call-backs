const fs = require('fs');
const { builtinModules } = require('module');

function getRandomStringArray(arrayLength, wordLength) {
    let outputArray = new Array(arrayLength).fill('');
    outputArray.forEach((randomString, index, array) => {
        array[index] = Math.random().toString(36).substring(2, wordLength);
    });
    return outputArray;
};

function createAndDeleteRandomJson(numberOfFiles) {
    let dir = './output';
    let data = 'Hello World!\n';
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);``
    }
    const fileNames = getRandomStringArray(numberOfFiles, 6);
    //console.log(fileNames);
    fileNames.forEach((file) => {
        let filePath = dir + '/' + file + '.json';
        fs.appendFile(filePath, data, 'utf8', function (err) {
            if (err) {
                console.log(err);
            } else {
                console.log(`file created ${filePath}`);
                 fs.unlink(filePath, function (err) {
                    if (err) {
                        console.error(err);
                    } else {
                        console.log(`deleted ${filePath}`);
                    }
                });
            }
        });
    })
}

module.exports = { createAndDeleteRandomJson }